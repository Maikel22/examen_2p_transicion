//Librerias importadas
import 'package:examen/models/themoviedbservice.dart';
import 'package:examen/ui/cajapelicula.dart';
import 'package:examen/ui/spinnerwidget.dart';
import 'package:flutter/material.dart';


void main() {
  runApp(MyApp());
}

class MyApp extends StatelessWidget {

  
  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      title: 'Flutter Demo',
      theme: ThemeData(
        primarySwatch: Colors.blue,
      ),
      //Nombre de la clase 
      home: MoviePage(),
    );
  }
}

//La clase MoviePage hereda de la clase StatefulWidget
class MoviePage extends StatelessWidget{

  @override
  Widget build(BuildContext context){
    
    //Devuelve widget de tipo Scaffold
    return Scaffold(
      appBar: AppBar(
        title: Text('Peliculas más valoradas'),
      ),
      //Uso del widget FutureBuilder
      body: FutureBuilder(
        //Datos inicial
        future: TheMovieDBService.getTopRatedMovies(),
        builder: 
          //Al realizar no recibe datos pasará por esta linea de espera, caso contrario pasara por la lista
          (BuildContext context, AsyncSnapshot<List> snapshot){
            //Si la variable snapshot tiene datos
            if (snapshot.hasData){
              //Retorna la lista
              return ListView.builder(      
                //Numero de los objetos          
                itemCount: snapshot.data!.length,
                //Recorre la lista
                itemBuilder: (context, index){
                  //Pelcula toma los valores del recorrido
                  var pelicula = snapshot.data![index];
                  //las peliculas pasan por caja pelicula
                  return CajaPelicula(peli: pelicula);
                },
              );
            } else {
              return SpinnerWidget();
            }
          },
      ),
    );

  }

}
  