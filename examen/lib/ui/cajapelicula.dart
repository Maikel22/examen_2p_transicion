//Clase para mostrar el detalle de las peliculas

//Librerias importadas
import 'package:flutter/material.dart';
import 'package:examen/models/movie.dart';
import 'package:examen/ui/pelicula.dart';

//Definir clase

class CajaPelicula extends StatelessWidget{

  //Declarar pelicula
  Movie peli;

  //Contructo para definir que el parametro es obligatorio
  CajaPelicula({required this.peli});
  
  @override
  Widget build(BuildContext context){
    //Devolver una lista de titulos
    return ListTile(
      //Titulo de la pelicula
      title: Text(this.peli.title),
      //Concatena la media de las votaciones más el numero de votos
      subtitle: Text(this.peli.voteAverage.toString() + " (" + this.peli.voteCount.toString() + ")"),
      //Imagen de la pelicula
      trailing: Image.network(this.peli.getImage()),
      //En caso de que pulsen 
      onTap: (){
        //Muestra todo los datos que se tiene de la pelicula
        Navigator.push(context, MaterialPageRoute(builder: 
        (context) => Pelicula (movie: this.peli)));
      },
    );
  }

}


